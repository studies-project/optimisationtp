import numpy as np
import matplotlib.pyplot as plt

def f(x1, x2) :
	return ((x1-3)**2) + ((x2+1)**2)

def df(x1, x2) :
	return (2 * (x1-3)), (2 * (x2+1))
	
def gradientDescente(defaultX1, defaultX2, sigma, target, error):
	gradient = []
	a = defaultX1
	b = defaultX2
	# We can't achieve the exact target so we use an error.
	while(f(a, b) > (target + error)):
		gradientX, gradientY = df(a, b)
		a = a - (sigma * gradientX)
		b = b - (sigma * gradientY)
		gradient.append([a, b])
	return gradient

"""
On veut appliquer l'algorithme des 1/5 pour notre algirhtme ES. Ici on doit utiliser une loi normale multi variée car notre fonction f1 prend deux paramètres.
"""
def unCinquiemeES(x1, x2, sigma, gamma, target, error, maxTurn):
	sigmaArray = []
	fArray = []
	mean = np.array([0, 0])
	mat = np.array([[1,0], [0, 1]])
	# On a besoin d'un compteur de tour
	count = 0
	while(f(x1, x2) > (target + error) and count < maxTurn):
		variation = np.random.multivariate_normal(mean, mat)
		y1 = x1 + sigma * variation[0]
		y2 = x2 + sigma * variation[1]

		if (f(y1, y2) < f(x1, x2)):
			x1, x2 = y1, y2
			sigma = sigma * gamma
		else:
			sigma = sigma * gamma**(-1/4)
		sigmaArray.append(sigma)
		fArray.append(f(x1, x2))

		count += 1
	return sigmaArray, fArray


"""
Version avec contrainte.
Différente façon de contrainte, soit on met valeur absolue (réparation), soit on fait de la sanction, si une de mes variables est négative j'applique une sanction sur f(x1, x2) qui va donc empêcher x d'être retenue.
"""
def unCinquiemeESContrainte(x1, x2, sigma, gamma, target, error, maxTurn):
	sigmaArray = []
	fArray = []
	mean = np.array([0, 0])
	mat = np.array([[1,0], [0, 1]])
	# On a besoin d'un compteur de tour
	count = 0
	while(f(x1, x2) > (target + error) and count < maxTurn):
		variation = np.random.multivariate_normal(mean, mat)
		y1 = x1 + sigma * variation[0]
		y2 = x2 + sigma * variation[1]

		# On applique une sanction au résultat
		if (y1 < 0 or y2 < 0):
			# Pour avoir une grande valeur entière
			sanction = np.iinfo(np.int32).max
		else :
			sanction = 0

		if (f(y1, y2) + sanction < f(x1, x2)):
			x1, x2 = y1, y2
			sigma = sigma * gamma
		else:
			sigma = sigma * gamma**(-1/4)
		sigmaArray.append(sigma)
		fArray.append(f(x1, x2))

		count += 1
	return sigmaArray, fArray

# Affichage pour le gradient
# plt.plot(gradientDescente(0, 0, 1e-2, 0, 1e-6))

# Affichage pour le un cinquième avec contrainte.
sigmaArray, fArray = unCinquiemeESContrainte(0, 0, 1, 1.2, 0, 1e-6, 200)
plt.plot(sigmaArray, color="blue", label="Sigma")
plt.plot(fArray, color="orange", label="f(x1, x2)")
plt.grid(True)
plt.legend(loc="upper right")
plt.show()