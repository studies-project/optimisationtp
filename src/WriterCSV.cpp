#include "WriterCSV.hpp"

WriterCSV::WriterCSV(string fileName)
{
	csvFile.open(fileName, ofstream::out);
	csvFile<<"nbeval; fitness\n";
}

void WriterCSV::writeLine(double numberOfTime, double bestCase)
{
	csvFile<<to_string(numberOfTime) + "; " + to_string(bestCase) + "\n";
}

void WriterCSV::closeFile()
{
	csvFile.close();
}