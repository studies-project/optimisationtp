#ifndef SOLUTION_HPP_
#define SOLUTION_HPP_

#include <vector>
#include <iostream>

using namespace std;

class Solution
{
	private:
		double fitness;
		vector <bool> x;

	public:
		Solution(int size);
		void binaryNeighbour();
		double getFitness();
		void setFitness(double fitness);
		void setRandomBool();
		vector <bool> getBool();
		void invertBool(int index);
		void print();
};

#endif