#ifndef WRITERCSV_HPP_
#define WRITERCSV_HPP_

#include <iostream>
#include <fstream>
using namespace std;

class WriterCSV
{
	private:
		ofstream csvFile;

	public:
		WriterCSV(string fileName);
		void writeLine(double numberOfTime, double bestCase);
		void closeFile();
};

#endif