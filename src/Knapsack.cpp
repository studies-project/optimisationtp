#include "Knapsack.hpp"

Knapsack::Knapsack(string fileName)
{
	fstream file;
	file.open(fileName, ios::in);
	if (file.is_open())
	{
		string tempSt;
		file >> tempSt;
		number = stoi(tempSt);
		for (int i=0; i<number; i++)
		{
			file >> tempSt;
			profit.push_back(stoi(tempSt));
		}
		for (int i=0; i<number; i++)
		{
			file >> tempSt;
			weight.push_back(stod(tempSt));
		}
		file >> tempSt;
		capacity = stod(tempSt);
		file.close();
	}
	else cout<<"Error opening file"<<endl;
}

void Knapsack::print()
{
	cout << "Number of element : "<<number << endl;
	cout << "Profit for each element : "<< endl;
	
	for (unsigned i=0; i<profit.size(); i++)
		cout<<"Element : "<<i<<" => "<<profit.at(i)<< endl;
	
	cout << "Weight for each element : "<< endl;

	for (unsigned i=0; i<weight.size(); i++)
		cout<<"Element : "<<i<<" => "<<weight.at(i)<< endl;
	
	cout << "Capacity : "<<capacity << endl;
}

int Knapsack::getNumber() { return number; }

double Knapsack::getCapacity() { return capacity; }

vector <int> Knapsack::getProfit() { return profit; }

vector <double> Knapsack::getWeight() { return weight; }

double Knapsack::auxEvalProfit(Solution & s)
{
	double sum = 0;
	vector <bool> x = s.getBool();

	for (unsigned i=0; i<profit.size(); i++)
		sum += profit.at(i) * x.at(i);

	return sum;
}

double Knapsack::auxEvalWeight(Solution & s)
{
	double sum = 0;
	vector <bool> x = s.getBool();

	for (unsigned i=0; i<weight.size(); i++)
		sum += weight.at(i) * x.at(i);

	return sum;
}

// Penality (keeps heavy weight).
double Knapsack::beta(Solution & s)
{
	double max = 0;
	vector <bool> x = s.getBool();

	for (unsigned i=0; i<weight.size(); i++)
	{
		// If the element is on the boolean array.
		if (x.at(i))
		{
			if (profit.at(i)/weight.at(i) > max)
				max = profit.at(i)/weight.at(i);
		}
	}

	return max;
}

double Knapsack::evaluate(Solution & s)
{
	if (auxEvalWeight(s) <= capacity)
		return auxEvalProfit(s);

	return auxEvalProfit(s) - beta(s) * (auxEvalWeight(s) - capacity);
}