#ifndef KNAPSACK_HPP_
#define KNAPSACK_HPP_

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include "Solution.hpp"

using namespace std;

class Knapsack
{
	private:
		int number;
		vector <int> profit;
		vector <double> weight;
		double capacity;

	public:
		Knapsack(string fileName);
		int getNumber();
		vector <int> getProfit();
		vector <double> getWeight();
		double getCapacity();
		void print();

		double auxEvalProfit(Solution & s);
		double auxEvalWeight(Solution & s);
		double beta(Solution & s);
		double evaluate(Solution & s);
};

#endif