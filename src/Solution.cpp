#include "Solution.hpp"
#include <cstdlib>
#include <ctime>

Solution::Solution(int number)
{
	fitness = 0;
	for (int i=0; i<number; i++)
		x.push_back(false);
	setRandomBool();
}


void Solution::binaryNeighbour()
{
	int index = rand() % x.size();
	x.at(index) = !x.at(index);
}

double Solution::getFitness() { return fitness; }

void Solution::setFitness(double fitness) { this->fitness = fitness; }

void Solution::setRandomBool()
{
	// srand(time(NULL));
	for (auto i : x)
		i = (rand() % 2);
}

vector <bool> Solution::getBool() { return x; }

void Solution::invertBool(int index)
{
	x.at(index) = !x.at(index);
}

void Solution::print()
{
	cout << "Solution" << endl;
	for (unsigned i=0; i<x.size(); i++)
		cout<<"Element : "<<i<<" => "<<x.at(i)<<endl;
}