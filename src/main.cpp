#include <iostream>
#include "Knapsack.hpp"
#include "Solution.hpp"
#include "WriterCSV.hpp"

using namespace std;

void launchEvaluation (double numberOfTime, WriterCSV & file)
{
	double bestCase = 0;
	for (int i=0; i<numberOfTime; i++)
	{
		Knapsack sack("ks_1000.dat.txt");
		Solution sol(sack.getNumber());
		if (sack.evaluate(sol) > bestCase)
			bestCase = sack.evaluate(sol);
	}
	file.writeLine(numberOfTime, bestCase);
}

void launchEvaluationRandomWalk (double numberOfTime, Knapsack & sack, Solution sol, WriterCSV & file)
{
	for (int i=0; i<numberOfTime; i++)
	{
		sol.binaryNeighbour();
		file.writeLine(i+1, sack.evaluate(sol));
	}
}

void launchBestImprovement (double numberOfTime, Knapsack & sack, Solution sol, WriterCSV & file)
{
	for (int i=0; i<numberOfTime; i++)
	{
		// We need to test here every solutions with different binaryNeighbours.
		double bestCase = 0;
		Solution originalSol = sol;
		Solution maxSol = sol;

		for (unsigned j=0; j<sol.getBool().size(); j++)
		{
			sol = originalSol;
			sol.invertBool(j);

			if (sack.evaluate(sol) > bestCase)
			{
				bestCase = sack.evaluate(sol);
				maxSol = sol;
			}
		}
		
		sol = maxSol;
		file.writeLine(i+1, bestCase);
	}
}

int main()
{
	// Need to put it here
	srand(time(NULL));
	WriterCSV writerCSVFile("hcbi.csv");
	Knapsack sack("../ks_1000.dat.txt");
	Solution sol(sack.getNumber());
	launchEvaluationRandomWalk(10, sack, sol, writerCSVFile);
	/*
	launchEvaluationRandomWalk(50, sack, sol, writerCSVFile);
	launchEvaluationRandomWalk(100, sack, sol, writerCSVFile);
	launchEvaluationRandomWalk(500, sack, sol, writerCSVFile);
	launchEvaluationRandomWalk(2000, sack, sol, writerCSVFile);
	launchEvaluationRandomWalk(5000, sack, sol, writerCSVFile);
	launchEvaluationRandomWalk(10000, sack, sol, writerCSVFile);
	launchEvaluationRandomWalk(50000, sack, sol, writerCSVFile);
	launchEvaluationRandomWalk(100000, sack, sol, writerCSVFile);
	launchBestImprovement(500, sack, sol, writerCSVFile);
	*/
	writerCSVFile.closeFile();
}